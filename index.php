<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		
    </head>
    <body>
		<input type="text" id="comment" value="">
		<input type="button" id="send" value="送信">
		
		<div id="main">
			
		</div>
		
		<script>
			$(function(){
				//初期処理
					$.ajax({
						type:"POST",
						url:"gets.php",
						dataType:"json"
					}).done(function(response){
						if(response.result === 'T'){
							drawPosts(response.data);
						}
					});
				
				//送信処理
				$("#send").click(function(){
				var comment = $("#comment").val();
				var url = "insert.php";
					$.ajax({
						type:"POST",
						url:url,
						dataType:"json",
						data:{
							comment: comment,
						}
					}).done(function(response){
						if(response.result === 'T'){
							var data = response.data;
								$("#main").prepend(data.id + ":" + data.comment + ":" + data.created + "<br>");
							
						}
					});
				});
				
			});
			var intervalID = window.setInterval(observer, 6000);
			function observer(){
				var url = "observer.php";
				$.ajax({
					type:"POST",
					url:url,
					dataType:"json"
				}).done(function(response){
					drawPosts(response.data);
				}).fail(function(response){
				});
			}
			function drawPosts(posts){
				$("#main").html("");
				for(var i=0;i<posts.length;i++){
					var data = posts[i];
					$("#main").prepend(data.id + ":" + data.comment + ":" + data.created + "<br>");
				}
			}
		</script>
			
    </body>
</html>
