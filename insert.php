<?php


if(!isset($_POST['comment']))exit("error");
include dirname(__FILE__) . '/lib/common.php';
$jsonResponder = new JsonResponder();

$comment = $_POST['comment'];
$postsDB = PostsDB::getInstance();
$created = date('Y-m-d H:i:s');
$res = $postsDB->insert(array('comment' => $comment, 'created' => $created));
if($res){
	$post = $postsDB->get(array('created' => $created));
	$jsonResponder->doneExit($post);
}else{
	$jsonResponder->failExit('');
}
