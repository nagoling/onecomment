<?php
//******************************************************************************
//　Debug
//******************************************************************************
ini_set('error_reporting', E_ALL);
ini_set('display_errors', '1');

//******************************************************************************
// Define
//******************************************************************************
function mainAutoloadNTicket($className) {
    if (file_exists(dirname(__FILE__) . "/{$className}.php")) {
        require_once (dirname(__FILE__) . "/{$className}.php");
    } else if (file_exists(dirname(__FILE__) . "/core/{$className}.php")) {
        require_once (dirname(__FILE__) . "/core/{$className}.php");
    }else if (file_exists(dirname(__FILE__) . "/filter/{$className}.php")) {
        require_once (dirname(__FILE__) . "/filter/{$className}.php");
    }else if (file_exists(dirname(__FILE__) . "/ope/{$className}.php")) {
        require_once (dirname(__FILE__) . "/ope/{$className}.php");
    } else if (file_exists(dirname(__FILE__) . "/lib/{$className}.php")) {
        require_once (dirname(__FILE__) . "/lib/{$className}.php");
    } else if (file_exists(dirname(__FILE__) . "/map/{$className}.php")) {
        require_once (dirname(__FILE__) . "/map/{$className}.php");
    } else if (file_exists(dirname(__FILE__) . "/nav/{$className}.php")) {
        require_once (dirname(__FILE__) . "/nav/{$className}.php");
    } else if (file_exists(dirname(__FILE__) . "/model/{$className}.php")) {
        require_once (dirname(__FILE__) . "/model/{$className}.php");
    } else if (file_exists(dirname(__FILE__) . "/db/{$className}.php")) {
        require_once (dirname(__FILE__) . "/db/{$className}.php");
	} else if (file_exists(dirname(__FILE__) . "/util/{$className}.php")) {
        require_once (dirname(__FILE__) . "/util/{$className}.php");
	}else if (file_exists(dirname(__FILE__) . "/static/{$className}.php")) {
        require_once (dirname(__FILE__) . "/static/{$className}.php");
	}else if (file_exists(dirname(__FILE__) . "/db/onecomment/{$className}.php")) {
        require_once (dirname(__FILE__) . "/db/onecomment/{$className}.php");
	}
}
/**
 * ajax用エラー設定
 * @param type $str
 */
function exit_status($str) {
    echo json_encode(array('status' => $str, 'result' => 'FAILED'));
    exit;
}
function h($text){
	return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}
spl_autoload_register('mainAutoloadNTicket');

