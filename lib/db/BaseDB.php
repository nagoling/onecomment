<?php
class BaseDB {
	/* @var $pdo PDO*/
	public $pdo;
	public $tableName;
	public $ignore = '';
	public static function getInstance(){
		;
	}
	/**
	 * PDOの取得
	 * @return PDO
	 */
	public function getPDO(){
		return $this->pdo;
	}
	public function isField($filedName){
		$stmt = $this->pdo->prepare("SHOW FIELDS FROM {$this->getTableName()};");
		$stmt->execute();
		$ret = array();
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			if($row["Field"] == $filedName){
				return true;
			}
		}
		return false;

	}
	public function isTableExists(){
		$q = "SHOW TABLE STATUS LIKE '%" . $this->getTableName() . "%'";
		$stmt = $this->pdo->prepare($q);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		return $row !== false;
	}
	public function insert($inArray){
		$keys = '';
		$value = '';
		$values = '';
		$valueArray = array();

		foreach($inArray as $key => $value){
			$keys .= $key . ',';
			$values .= '?,';
			$valueArray[] = $value;
		}
		$keys = rtrim($keys, ',');
		$values = rtrim($values, ',');
		
		$q = 'insert into ' . $this->getTableName() . '
                   (' . $keys . ')
            values (' . $values . ')
           ';

		/** @var $stmt PDOStatement */
		$stmt = $this->pdo->prepare($q);

		return $stmt->execute($valueArray);
	}
	public function beginTransaction(){
		return $this->pdo->beginTransaction();
	}
	public function commit(){
		return $this->pdo->commit();
	}
	public function rollBack(){
		return $this->pdo->rollBack();
	}
	/**
	 * 汎用update
	 * @param array $inUpdateArray updateしたいフィールド名を連想配列で渡す。例：array('name' => 'taro', 'email' => 'hoge@com.com')
	 * @param array $inTargetArray update対象としたいフィールド名を連想配列で渡す。複数の場合は間にorかandを入れる。例 array('id' => 10 , 'and', 'title' => 'kikikaikai')
	 * @return boolean 成功/可否
	 */
	public function update($inUpdateArray, $inTargetArray){
		//■update対象となっているフィールドの処理
		$value = '';
		$values = '';
		$valueArray = array();
		foreach($inUpdateArray as $key => $value){
			$values .= $key . ' = ?,';
			$valueArray[] = $value;
		}
		$values = rtrim($values, ',');

		//■WHERE 以降の処理
		$whereAfter = '';
		$valueArray2 = array();
		foreach($inTargetArray as $key => $value){
			if(strtolower($value) == 'and' && is_int($key)){
				$whereAfter .= ' AND ';
			}else if(strtolower($value) == 'or' && is_int($key)){
				$whereAfter .= ' OR ';
			}else if(is_array($value)){
				foreach($value as $k1 => $v1){
					$whereAfter .= $k1 . ' = ?';
					$valueArray[] = $v1;
					//何があっても一回しかループしない
					break;
				}
			}else{
				$whereAfter .= $key . ' = ?';
				$valueArray2[] = $value;
			}
		}

		$q = "update {$this->getTableName()} set $values WHERE " . $whereAfter;
		$stmt = $this->pdo->prepare($q);
		try{
			return $stmt->execute( array_merge($valueArray, $valueArray2) );
		}catch(PDOException $e){
			echo $e->getMessage();
			exit;
		}
	}
	public function getAll($inAfterQuery=''){
		$q = "SELECT * from {$this->getTableName()} WHERE 1=1 " . $inAfterQuery;
		$stmt = $this->pdo->prepare($q);
		try{
			$stmt->execute();
			$ret = array();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$ret[] = $row;
			}
			return $ret;
		}catch(PDOException $e){
			echo $e->getMessage();
			exit;
		}
	}
	public function gets($inTargetArray, $inAfterQuery='', $fields = null){
		//■WHERE 以降の処理
		
		$whereAfter = '';
		$valueArray = array();
		foreach($inTargetArray as $key => $value){
			if(is_array($value) && count($value) === 3){
					$whereAfter .= $value[0] . ' ' . $value[1] . ' ' . ' ? ';
					$valueArray[] = $value[2];
			}
			else if(is_array($value) && strtolower($value[0]) == 'and'){
				$whereAfter .= ' AND ';
			}
				else if(strtolower($value) == 'and' && is_int($key)){
				$whereAfter .= ' AND ';
			}
			else if(strtolower($value) == 'or' && is_int($key)){
				$whereAfter .= ' OR ';
			}else if(is_array($value)){
				foreach($value as $k1 => $v1){
					$whereAfter .= $k1 . ' = ? ';
					$valueArray[] = $v1;
					//何があっても一回しかループしない
					break;
				}
			}
			else{
				$whereAfter .= $key . ' = ? ';
				$valueArray[] = $value;
			}
		}
		$fieldsSql = '*';
		if($fields && is_array($fields)){
			$fieldsSql = '';
			foreach($fields as $field){
				$fieldsSql .= $field . ',';
			}
			$fieldsSql = rtrim($fieldsSql, ',');
		}
		if(!$whereAfter && !$inAfterQuery) $whereAfter = 1;
//		if(!$inAfterQuery)$inAfterQuery = ' ' . $inAfterQuery;
		if(!$whereAfter) $whereAfter = 1;
		$sql = "SELECT {$fieldsSql} from {$this->getTableName()} WHERE " . $whereAfter . $inAfterQuery;
		$stmt = $this->pdo->prepare($sql);
		try{
			$res = $stmt->execute( $valueArray );
			$ret = array();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$ret[] = $row;
			}
			return $ret;
		}catch(PDOException $e){
//			echo "query:$q<br>";
			echo $e->getMessage();
			exit;
		}
	}
	public function get($inTargetArray, $inAfterQuery=''){
		$ret = $this->gets($inTargetArray, $inAfterQuery);
		if(!$ret || count($ret) == 0)return false;
		return $ret[0];
	}
	public function getBindParam($inValue){
		switch(true){
			case is_bool($inValue) :
				return PDO::PARAM_BOOL;
			case is_null($inValue) :
				return PDO::PARAM_NULL;
			case is_int($inValue) :
				return PDO::PARAM_INT;
			case is_float($inValue) :
			case is_numeric($inValue) :
			case is_string($inValue) :
			default:
				return PDO::PARAM_STR;
		}
	}
	public function errorInfo(){
		return var_export(ReafDB::getPDO()->errorInfo(), true);
	}
	public function lock(){
		$this->pdo->query("LOCK TABLES {$this->getTableName()} READ;");
	}
	public function unlock(){
		$this->pdo->query('UNLOCK TABLES;');
	}
	final public function __clone(){
		throw new Exception("you can't clone this object");
	}
	public function setIgnore(){
		$this->ignore = ' ignore ';
	}
	public function unsetIgnore(){
		$this->ignore = '';
	}
	public function delete($param, $value) {
		$q = "delete from {$this->getTableName()} where $param = ?";
		$stmt = $this->pdo->prepare($q);
		return $stmt->execute(array($value));
	}
	public function deleteAll($param, $array) {
		$where = '';
		foreach($array as $value){
			$where .= " {$param} = ? OR";
		}
		if(!$where)return false;
		$where = rtrim($where, 'OR');
		$q = "delete from {$this->getTableName()} where {$where}";
		$stmt = $this->pdo->prepare($q);
		return $stmt->execute($array);
	}
	public function is($param, $value){
		$tmp = $this->get(array($param => $value));
		return $tmp !== false;
	}
	public function getTableName(){
		return ConnectDB::$prefix . $this->tableName;
	}
}
