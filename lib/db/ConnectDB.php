<?php


class ConnectDB
{
	private static $pdo = null;
	private static $pdoArray = null;
	public static $prefix = '';
	static function connect($key)
	{
		if($_SERVER['SERVER_NAME'] == 'localhost'){
			$settings = array(
				'onecomment' => array(
					'user' => 'root',
					'password' => '',
					'database' => 'onecomment',
					'host' => 'localhost',
				),
			);
		}else{
			
		}
			
		if(isset(self::$pdoArray[$key])){
			return self::$pdoArray[$key];
		}
		$pdo = self::$pdoArray[$key];
		$dbName = $settings[$key]['database'];
		$dbUser = $settings[$key]['user'];
		$dbPassword = $settings[$key]['password'];
		$dbHost = $settings[$key]['host'];
		if(self::$pdo)return self::$pdo;
		try{
			// MySQLへ接続
			self::$pdo = new PDO(
				"mysql:host={$dbHost}; dbname=".$dbName,
				$dbUser,
				$dbPassword
			);
		}
		catch(PDOException $e){
			var_dump($e->getMessage());
			return NULL;
		}
		// 文字コード設定
		self::$pdo->exec( 'SET NAMES utf8;' );
		return self::$pdo;
	}
};

