<?php
class PostsDB extends OneCommentBaseDB{
	private function  __construct() {
		$this->init('posts');
	}
	/**
	 *
	 * @staticvar boolean $instance
	 * @return PostsDB
	 */
	public static function getInstance() {
		static $instance = false;
		if(!$instance){
			$instance = new self();
		}
		return $instance;
	}
}