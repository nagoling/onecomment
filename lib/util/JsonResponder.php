<?php
/**
 * Description of JsonResponder
 *
 * @author nagomi
 */
class JsonResponder {
	private $option;
	public function __construct() {
//		$this->option = JSON_UNESCAPED_UNICODE | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP;
		$this->option = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP;
		$this->init();
	}
	protected function init(){
		header('Content-Type: application/json');
		header('X-Content-Type-Options: nosniff');
		if ($_SERVER['HTTP_X_REQUESTED_WITH'] !== 'XMLHttpRequest') {
			$this->failExit('不正な呼び出しです' );
		}
		if (strtolower($_SERVER['REQUEST_METHOD'] != 'POST')) {
			$this->failExit('Error! Wrong HTTP method!  :' . filter_input( INPUT_SERVER, 'REQUEST_METHOD'));
		}
	}
	public function doneExit($data){
		echo $this->done($data);
		exit;
	}
	public function failExit($data){
		echo $this->fail($data);
		exit;
	}
	public function done($data){
		return json_encode(array(
			'result' => 'T',
			'data' => $data,
		), $this->option);
	}
	public function fail($msg){
		return json_encode(array(
			'result' => 'F',
			'status' => $msg,
		), $this->option);
	}
}
